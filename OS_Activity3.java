import java.util.Scanner;
import java.util.*;

// By Rainiery Tavarez, Murtaza Nipplewala, Suraj Gurung

class Activity_3 {

    static int[][] mat = new int[10][6];

    // Driver Code
    public static void main(String[] args) {
        int choice = 0;
        int selection = 0;
        Scanner sc = new Scanner(System.in);
        /*do {
            System.out.println("Enter 1 for Short Job");
            System.out.println("Enter 2 for Round Robin");
            choice = sc.nextInt();
            if (choice == 1) {
                shortJobNext();
                //sc.close();
            } else if (choice == 2) {
                roundRobin();
            } else {
                System.out.println("Invalid Input");
            }
            System.out.println("Do you wish to continue (1 for yes)");
            selection = sc.nextInt();
        }while (selection == 1);*/
        System.out.println("Enter 1 for Short Job");
        System.out.println("Enter 2 for Round Robin");
        choice = sc.nextInt();
        if (choice == 1) {
            shortJobNext();
            //sc.close();
        } else if (choice == 2) {
            roundRobin();
        }
    }

    public static void shortJobNext(){
        int num;

        Scanner sc = new Scanner(System.in);

        System.out.println("Enter number of Process: ");
        num = sc.nextInt();

        System.out.println("...Enter the process ID...");
        for (int i = 0; i < num; i++) {
            System.out.println("...Process " + (i + 1) + "...");
            System.out.println("Enter Process Id: ");
            mat[i][0] = sc.nextInt();
            System.out.println("Enter Arrival Time: ");
            mat[i][1] = sc.nextInt();
            System.out.println("Enter Burst Time: ");
            mat[i][2] = sc.nextInt();
        }

        System.out.println("Before Arrange...");
        System.out.println("Process ID\tArrival Time\tBurst Time");
        for (int i = 0; i < num; i++) {
            System.out.printf("%d\t\t%d\t\t%d\n",
                    mat[i][0], mat[i][1], mat[i][2]);
        }

        arrangeArrival(num, mat);
        completionTime(num, mat);
        System.out.println("Final Result...");
        System.out.println("Process ID\tArrival Time\tBurst" +
                " Time\tWaiting Time\tTurnaround Time");
        for (int i = 0; i < num; i++) {
            System.out.printf("%d\t\t\t%d\t\t\t\t%d\t\t\t%d\t\t\t%d\n",
                    mat[i][0], mat[i][1], mat[i][2], mat[i][4], mat[i][5]);
        }
        sc.close();
    }

    public static void roundRobin(){
        // process id's
        int processes[] = { 1, 2, 3, 4};
        int n = processes.length;

        // Burst time of all processes
        int burst_time[] = {10, 5, 8, 3};

        // Time slice
        int slice = 5;
        findavgTime(processes, n, burst_time, slice);
    }

    // SJN method
    static void arrangeArrival(int num, int[][] mat) {
        for (int i = 0; i < num; i++) {
            for (int j = 0; j < num - i - 1; j++) {
                if (mat[j][1] > mat[j + 1][1]) {
                    for (int k = 0; k < 5; k++) {
                        int temp = mat[j][k];
                        mat[j][k] = mat[j + 1][k];
                        mat[j + 1][k] = temp;
                    }
                }
            }
        }
    }

    // SJN method
    static void completionTime(int num, int[][] mat) {
        int temp, val = -1;
        mat[0][3] = mat[0][1] + mat[0][2];
        mat[0][5] = mat[0][3] - mat[0][1];
        mat[0][4] = mat[0][5] - mat[0][2];

        for (int i = 1; i < num; i++) {
            temp = mat[i - 1][3];
            int low = mat[i][2];
            for (int j = i; j < num; j++) {
                if (temp >= mat[j][1] && low >= mat[j][2]) {
                    low = mat[j][2];
                    val = j;
                }
            }
            mat[val][3] = temp + mat[val][2];
            mat[val][5] = mat[val][3] - mat[val][1];
            mat[val][4] = mat[val][5] - mat[val][2];
            for (int k = 0; k < 6; k++) {
                int tem = mat[val][k];
                mat[val][k] = mat[i][k];
                mat[i][k] = tem;
            }
        }
    }

    // RR method
    // Method to find the waiting time for all
    // processes
    static void findWaitingTime(int processes[], int n,
                                int bt[], int wt[], int slice)
    {
        // Make a copy of burst times bt[] to store remaining
        // burst times.
        int rem_bt[] = new int[n];
        for (int i = 0 ; i < n ; i++)
            rem_bt[i] =  bt[i];

        int t = 0; // Current time

        // Keep traversing processes in round robin manner
        // until all of them are not done.
        while(true)
        {
            boolean done = true;

            // Traverse all processes one by one repeatedly
            for (int i = 0 ; i < n; i++)
            {
                // If burst time of a process is greater than 0
                // then only need to process further
                if (rem_bt[i] > 0)
                {
                    done = false; // There is a pending process

                    if (rem_bt[i] > slice)
                    {
                        // Increase the value of t i.e. shows
                        // how much time a process has been processed
                        t += slice;

                        // Decrease the burst_time of current process
                        // by quantum
                        rem_bt[i] -= slice;
                    }

                    // If burst time is smaller than or equal to
                    // quantum. Last cycle for this process
                    else
                    {
                        // Increase the value of t i.e. shows
                        // how much time a process has been processed
                        t = t + rem_bt[i];

                        // Waiting time is current time minus time
                        // used by this process
                        wt[i] = t - bt[i];

                        // As the process gets fully executed
                        // make its remaining burst time = 0
                        rem_bt[i] = 0;
                    }
                }
            }

            // If all processes are done
            if (done == true)
                break;
        }
    }

    // RR method
    // Method to calculate turn around time
    static void findTurnAroundTime(int processes[], int n,
                                   int bt[], int wt[], int tat[])
    {
        // calculating turnaround time by adding
        // bt[i] + wt[i]
        for (int i = 0; i < n ; i++)
            tat[i] = bt[i] + wt[i];
    }

    // RR method
    // Method to calculate average time
    static void findavgTime(int processes[], int n, int bt[],
                            int slice)
    {
        int wt[] = new int[n], tat[] = new int[n];
        int total_wt = 0, total_tat = 0;

        // Function to find waiting time of all processes
        findWaitingTime(processes, n, bt, wt, slice);

        // Function to find turn around time for all processes
        findTurnAroundTime(processes, n, bt, wt, tat);

        // Display processes along with all details
        System.out.println("Processes " + " Burst time " +
                " Waiting time " + " Turn around time");

        // Calculate total waiting time and total turn
        // around time
        for (int i=0; i<n; i++)
        {
            total_wt = total_wt + wt[i];
            total_tat = total_tat + tat[i];
            System.out.println(" " + (i+1) + "\t\t" + bt[i] +"\t " +
                    wt[i] +"\t\t " + tat[i]);
        }

        System.out.println("Average waiting time = " +
                (float)total_wt / (float)n);
        System.out.println("Average turn around time = " +
                (float)total_tat / (float)n);
    }
}